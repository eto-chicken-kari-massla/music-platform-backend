create table playlists
(
    id                 int identity not null,
    name               nvarchar(255) not null,
    user_id            int not null,
    created_by         nvarchar(255),
    created_date       datetime2,
    deleted            bit,
    last_modified_by   nvarchar(255),
    last_modified_date datetime2,
    version            int,
    primary key(id)
);

create table playlist_tracks
(
    playlist_id int not null,
    track_id  int not null,
    primary key (playlist_id, track_id)
);

alter table playlist_tracks
    add constraint fk_playlist_tracks_user foreign key (playlist_id) references playlists

alter table playlist_tracks
    add constraint fk_playlist_tracks_track foreign key (track_id) references tracks

alter table playlists
    add constraint fk_playlists_user foreign key (user_id) references users
