create table artists_subscriptions
(
    artist_id    int not null references users (id),
    user_id int not null references users (id),
    primary key (artist_id, user_id)
);