package com.khramykh.platform.api.controllers;

import com.khramykh.platform.application.commons.sort.PlaylistSort;
import com.khramykh.platform.application.playlistsApi.PlaylistsService;
import com.khramykh.platform.application.playlistsApi.commands.PlaylistCreateCommand;
import com.khramykh.platform.application.playlistsApi.commands.PlaylistUpdateCommand;
import com.khramykh.platform.domain.entities.Playlist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/playlists")
public class PlaylistsController {
    @Autowired
    PlaylistsService playlistsService;

    @GetMapping
    public ResponseEntity getAllByUser(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam int pageNum,
            @RequestParam int pageSize,
            @RequestParam PlaylistSort playlistSort) {
        if (userDetails != null) {
            Page<Playlist> playlistPage = playlistsService.getPlaylistsByUser(pageNum, pageSize, userDetails.getUsername(), playlistSort);
            return ResponseEntity.ok().body(playlistPage);
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

    }

    @GetMapping("{id}")
    public ResponseEntity getOneById(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable int id) {
        if (userDetails != null) {
            Playlist playlist = playlistsService.getPlaylistById(id, userDetails.getUsername());
            return ResponseEntity.ok().body(playlist);
        } else
            return ResponseEntity.badRequest().build();
    }

    @PostMapping
    public ResponseEntity create(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestBody PlaylistCreateCommand command) {
        Playlist created;
        if (userDetails != null) {
            created = playlistsService.create(command, userDetails.getUsername());
            return ResponseEntity.ok().body(created);
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(
            @PathVariable int id,
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        if (userDetails == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        playlistsService.removeById(id, userDetails.getUsername());
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public ResponseEntity update(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestBody PlaylistUpdateCommand command
    ) {
        if (userDetails != null) {
            Playlist updated = playlistsService.update(command, userDetails.getUsername());
            return ResponseEntity.ok().body(updated);
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @GetMapping("addTrack/{playlistId}/{trackId}")
    public ResponseEntity addTrackToPlaylist(
            @PathVariable int playlistId,
            @PathVariable int trackId,
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        if (userDetails != null) {
            playlistsService.addTrackToPlaylist(playlistId, trackId, userDetails.getUsername());
            return ResponseEntity.ok().build();
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @GetMapping("removeTrack/{playlistId}/{trackId}")
    public ResponseEntity removeTrackFromPlaylist(
            @PathVariable int playlistId,
            @PathVariable int trackId,
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        if (userDetails != null) {
            playlistsService.removeTrackFromPlaylist(playlistId, trackId, userDetails.getUsername());
            return ResponseEntity.ok().build();
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
