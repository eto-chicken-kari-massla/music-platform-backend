package com.khramykh.platform.api.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.khramykh.platform.api.commons.JsonPage;
import com.khramykh.platform.api.commons.JsonViews;
import com.khramykh.platform.application.artistsApi.ArtistsService;
import com.khramykh.platform.application.artistsApi.commands.ArtistCreateCommand;
import com.khramykh.platform.application.artistsApi.commands.ArtistUpdateCommand;
import com.khramykh.platform.application.commons.behaviors.UserPrincipal;
import com.khramykh.platform.application.commons.sort.ArtistSort;
import com.khramykh.platform.domain.entities.Artist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

@RestController
@RequestMapping(value = "/api/artists", produces = { "application/json" })
public class ArtistsController {
    @Autowired
    ArtistsService artistsService;

    @GetMapping
    @JsonView(JsonViews.UI.ArtistView.class)
    public ResponseEntity getAll(
            @RequestParam int pageNum,
            @RequestParam int pageSize,
            @RequestParam(required = false) String filter,
            @RequestParam ArtistSort artistSort) {
        Page<Artist> artistsPage;
        if (filter != null) {
            artistsPage = artistsService.getArtistByName(filter, pageNum, pageSize, artistSort);
        } else {
            artistsPage = artistsService.getArtistsByPage(pageNum, pageSize, artistSort);
        }
        return ResponseEntity.ok().body(new JsonPage(artistsPage));
    }

    @GetMapping("{id}")
    @JsonView(JsonViews.UI.ArtistView.class)
    public ResponseEntity getOneById(@PathVariable int id) {
        Artist artist = artistsService.getArtistById(id);
        return ResponseEntity.ok().body(artist);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity delete(@PathVariable int id) {
        artistsService.removeById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity update(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam(name = "id") int id,
            @RequestParam(name = "name") String name,
            @RequestParam(name = "description") String description,
            @RequestParam(name = "createdDate") String createdDate,
            @RequestParam(name = "photoFile", required = false) MultipartFile photoFile
    ) throws ParseException, IOException {
        ArtistUpdateCommand command = new ArtistUpdateCommand();
        command.setId(id);
        command.setName(name);
        command.setDescription(description);
        command.setCreatedDate(createdDate);
        command.setPhotoFile(photoFile);
        Artist updated = artistsService.update(command, userDetails.getUsername());
        return ResponseEntity.ok().body(updated);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity create(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam(name = "name") String name,
            @RequestParam(name = "description") String description,
            @RequestParam(name = "createdDate") String createdDate,
            @RequestParam(name = "photoFile", required = false) MultipartFile photoFile
    ) throws ParseException, IOException {
        ArtistCreateCommand command = new ArtistCreateCommand();
        command.setName(name);
        command.setDescription(description);
        command.setCreatedDate(createdDate);
        command.setPhotoFile(photoFile);
        Artist created = artistsService.create(command, userDetails.getUsername());
        return ResponseEntity.ok().body(created);
    }

    @GetMapping("/subscribe")
    public ResponseEntity like(@AuthenticationPrincipal UserPrincipal userPrincipal, @RequestParam int artistId) {
        if(userPrincipal != null) {
            artistsService.subscribe(userPrincipal.getId(), artistId);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping("/unsubscribe")
    public ResponseEntity dislike(@AuthenticationPrincipal UserPrincipal userPrincipal, @RequestParam int artistId) {
        if(userPrincipal != null) {
            artistsService.unsubscribe(userPrincipal.getId(), artistId);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
