package com.khramykh.platform.api.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.khramykh.platform.api.commons.JsonViews;
import com.khramykh.platform.application.searchApi.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/search")
public class SearchController {
    @Autowired
    private SearchService searchService;

    @GetMapping
    @JsonView({JsonViews.UI.SearchView.class})
    public ResponseEntity search(@RequestParam String q) {
        return ResponseEntity.ok().body(searchService.search(q));
    }
}
