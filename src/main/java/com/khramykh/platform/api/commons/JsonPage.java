package com.khramykh.platform.api.commons;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

public class JsonPage<T> implements Page<T> {

    private Page<T> pageObj;

    public JsonPage(Page<T> pageObj) {
        this.pageObj = pageObj;
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public int getNumber() {
        return pageObj.getNumber();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public int getSize() {
        return pageObj.getSize();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public int getNumberOfElements() {
        return pageObj.getNumberOfElements();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public List<T> getContent() {
        return pageObj.getContent();
    }

    @Override
    public boolean hasContent() {
        return pageObj.hasContent();
    }

    @Override
    public Sort getSort() {
        return pageObj.getSort();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public boolean isFirst() {
        return pageObj.isFirst();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public boolean isLast() {
        return pageObj.isLast();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public boolean hasNext() {
        return pageObj.hasNext();
    }

    @Override
    @JsonView(JsonViews.UI.class)
    public boolean hasPrevious() {
        return pageObj.hasPrevious();
    }

    @Override
    public Pageable nextPageable() {
        return pageObj.nextPageable();
    }

    @Override
    public Pageable previousPageable() {
        return pageObj.previousPageable();
    }

    @Override
    public Iterator<T> iterator() {
        return pageObj.iterator();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public int getTotalPages() {
        return pageObj.getTotalPages();
    }

    @JsonView(JsonViews.UI.class)
    @Override
    public long getTotalElements() {
        return pageObj.getTotalElements();
    }
    @Override
    public <U> Page<U> map(Function<? super T, ? extends U> function) {
        return pageObj.map(function);
    }

}