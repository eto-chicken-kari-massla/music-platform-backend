package com.khramykh.platform.application.commons.sort;

public enum PlaylistSort {
    ID_DESC, ID_ASC,
    NAME_DESC, NAME_ASC
}
