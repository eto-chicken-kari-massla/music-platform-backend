package com.khramykh.platform.application.commons.utils;

import com.google.cloud.storage.*;
import com.khramykh.platform.application.utils.ImageHelper;
import com.khramykh.platform.application.utils.ImageSize;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@Component
public class FileHelper {

    @Value("${google.storage.projectId}")
    private String projectId;

    @Value("${google.storage.bucketName}")
    private String bucketName;

    @Value("${google.storage.bucketUrl}")
    private String bucketUrl;

    @Value("${upload.path}")
    private String uploadPath;

    public String getNewUri(MultipartFile file, FileOperations type) throws IOException {
        if (file != null && !Objects.requireNonNull(file.getOriginalFilename()).isEmpty()) {
//            String pathToUpdate = uploadPath;
            String pathToUpdate = "";
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename().trim();
            switch (type) {
                case TRACK_FILE:
                    pathToUpdate += "tracks/";
                    break;
                case TRACK_PHOTO:
                    pathToUpdate += "images/tracks/";
                    break;
                case USER_PHOTO:
                    pathToUpdate += "images/users/";
                    break;
                case ARTIST_PHOTO:
                    pathToUpdate += "images/artists/";
                    break;
                case ALBUM_PHOTO:
                    pathToUpdate += "images/albums/";
                    break;
            }

            BufferedImage bufferedImage = null;
            switch (type) {
                case TRACK_PHOTO:
                    bufferedImage = ImageHelper.compress(file, ImageSize.TRACK_PHOTO_HEIGHT);
                    break;
                case USER_PHOTO:
                    bufferedImage = ImageHelper.compress(file, ImageSize.PROFILE_PHOTO_HEIGHT);
                    break;
                case ARTIST_PHOTO:
                    bufferedImage = ImageHelper.compress(file, ImageSize.ARTIST_PHOTO_HEIGHT);
                    break;
                case ALBUM_PHOTO:
                    bufferedImage = ImageHelper.compress(file, ImageSize.ALBUM_PHOTO_HEIGHT);
                    break;
            }
            if (bufferedImage != null) {
//                ImageHelper.saveImage(bufferedImage, uploadDir.getPath() + "/" + resultFilename, type);
                resultFilename = saveImageToGoogleStorage(bufferedImage, pathToUpdate + resultFilename);
            } else {
                resultFilename = saveTrackToGoogleStorage(file, pathToUpdate + resultFilename);
            }


            return resultFilename;
        } else {
            return Strings.EMPTY;
        }
    }

    public void deleteFile(String fileUri, FileOperations type) {
        String pathToDelete = uploadPath;
        switch (type) {
            case TRACK_FILE:
                pathToDelete += "/tracks/";
                break;
            case TRACK_PHOTO:
                pathToDelete += "/images/tracks/";
                break;
            case USER_PHOTO:
                pathToDelete += "/images/users/";
                break;
            case ARTIST_PHOTO:
                pathToDelete += "/images/artists/";
                break;
            case ALBUM_PHOTO:
                pathToDelete += "/images/albums/";
                break;
        }
        File uploadDir = new File(pathToDelete + "/" + fileUri);

        if (uploadDir.exists()) {
            uploadDir.delete();
        }
    }

    private String saveImageToGoogleStorage(BufferedImage bufferedImage, String filePath) throws IOException {
        String path = bucketUrl + filePath;
        Storage storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();
        BlobId blobId = BlobId.of(bucketName, filePath);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, filePath.substring(filePath.lastIndexOf(".") + 1), baos);
        byte[] bytes = baos.toByteArray();
        storage.create(blobInfo, bytes);
        return path;
    }

    private String saveTrackToGoogleStorage(MultipartFile file, String filePath) throws IOException {
        String path = bucketUrl + filePath;
        Storage storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();
        BlobId blobId = BlobId.of(bucketName, filePath);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
        storage.create(blobInfo, file.getBytes());
        return path;
    }
}
