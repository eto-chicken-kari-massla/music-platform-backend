package com.khramykh.platform.application.commons.utils;

public enum FileOperations {
    USER_PHOTO,
    TRACK_PHOTO,
    TRACK_FILE,
    ALBUM_PHOTO,
    ARTIST_PHOTO
}
