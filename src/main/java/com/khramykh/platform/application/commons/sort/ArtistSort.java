package com.khramykh.platform.application.commons.sort;

public enum ArtistSort {
    ID_DESC, ID_ASC,
    NAME_DESC, NAME_ASC,
    DESCRIPTION_DESC, DESCRIPTION_ASC,
    CREATEDDATE_DESC, CREATEDDATE_ASC,
}
