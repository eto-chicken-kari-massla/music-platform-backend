package com.khramykh.platform.application.commons.sort;

public enum AlbumSort {
    ID_DESC, ID_ASC,
    NAME_DESC, NAME_ASC,
    RELEASEDATE_DESC, RELEASEDATE_ASC,
    DESCRIPTION_DESC, DESCRIPTION_ASC,
    TYPE_DESC, TYPE_ASC,
}
