package com.khramykh.platform.application.commons.sort;

public enum TrackSort {
    ID_DESC, ID_ASC,
    NAME_DESC, NAME_ASC,
    RELEASEDATE_DESC, RELEASEDATE_ASC,
    TYPE_DESC, TYPE_ASC,
    DESCRIPTION_DESC, DESCRIPTION_ASC,
    CATEGORY_DESC, CATEGORY_ASC,
    ALBUM_DESC, ALBUM_ASC,
}
