package com.khramykh.platform.application.commons.sort;

public enum UserSort {
    ID_DESC, ID_ASC,
    EMAIL_DESC, EMAIL_ASC,
    FIRSTNAME_DESC, FIRSTNAME_ASC,
    LASTNAME_DESC, LASTNAME_ASC,
    BIRTHDAY_DESC, BIRTHDAY_ASC,
    COUNTRY_DESC, COUNTRY_ASC,
    ROLES_DESC, ROLES_ASC,
    GENDER_DESC, GENDER_ASC,
}
