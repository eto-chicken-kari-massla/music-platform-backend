package com.khramykh.platform.application.commons.sort;

public enum CategorySort {
    NAME_DESC, NAME_ASC,
    ID_DESC, ID_ASC,
    DESCRIPTION_DESC, DESCRIPTION_ASC
}
