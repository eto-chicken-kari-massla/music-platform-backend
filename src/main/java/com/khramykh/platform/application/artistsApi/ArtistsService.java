package com.khramykh.platform.application.artistsApi;

import com.khramykh.platform.application.artistsApi.commands.ArtistCreateCommand;
import com.khramykh.platform.application.artistsApi.commands.ArtistUpdateCommand;
import com.khramykh.platform.application.commons.sort.ArtistSort;
import com.khramykh.platform.application.commons.utils.FileHelper;
import com.khramykh.platform.application.commons.utils.FileOperations;
import com.khramykh.platform.application.exceptions.ResourceNotFoundException;
import com.khramykh.platform.application.repositories.AlbumsRepository;
import com.khramykh.platform.application.repositories.ArtistsRepository;
import com.khramykh.platform.application.repositories.TracksRepository;
import com.khramykh.platform.application.repositories.UsersRepository;
import com.khramykh.platform.application.utils.MailSender;
import com.khramykh.platform.domain.entities.Artist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Service
public class ArtistsService {
    @Autowired
    ArtistsRepository artistsRepository;
    @Autowired
    AlbumsRepository albumsRepository;
    @Autowired
    TracksRepository tracksRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    FileHelper fileHelper;
    @Autowired
    MailSender mailSender;

    public Artist getArtistById(int id) {
        return artistsRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public Page<Artist> getArtistByName(String name, int pageNum, int pageSize, ArtistSort artistSort) {
        return artistsRepository.findByNameContaining(name, PageRequest.of(pageNum, pageSize, getSortType(artistSort)));
    }

    public Page<Artist> getArtistsByPage(int pageNum, int pageSize, ArtistSort artistSort) {
        return artistsRepository.findAll(PageRequest.of(pageNum, pageSize, getSortType(artistSort)));
    }

    public void removeById(int id) {
        if (!artistsRepository.existsById(id)) {
            throw new ResourceNotFoundException(id);
        }
        Artist artist = artistsRepository.findById(id).get();
        tracksRepository.deleteAllByArtistsContains(artist);
        albumsRepository.deleteAllByArtistsContains(artist);
        artistsRepository.deleteById(id);
    }

    public Artist update(ArtistUpdateCommand command, String lastModifiedBy) throws ParseException, IOException {
        Artist oldArtist = artistsRepository.findById(command.getId()).orElseThrow(() -> new ResourceNotFoundException((command.getId())));
        Artist updated = artistsRepository.save(convertArtistUpdateCommandToArtist(oldArtist, command));
        updated.setLastModifiedBy(lastModifiedBy);
        return updated;
    }

    public Artist create(ArtistCreateCommand command, String createdBy) throws ParseException, IOException {
        Artist artist = new Artist();
        artist.setName(command.getName());
        artist.setDescription(command.getDescription());
        if (command.getPhotoFile() != null) {
            artist.setPhotoUri(fileHelper.getNewUri(command.getPhotoFile(), FileOperations.ARTIST_PHOTO));
        }
        artist.setCreatedDate(new SimpleDateFormat("yyyy-MM-dd").parse(command.getCreatedDate()));
        artist.setCreatedBy(createdBy);
        artist.setLastModifiedBy(createdBy);
        artistsRepository.save(artist);
        return artist;
    }

    private Sort getSortType(ArtistSort artistSort) {
        switch (artistSort) {
            case NAME_DESC:
                return Sort.by("name").descending();
            case ID_ASC:
                return Sort.by("id").ascending();
            case ID_DESC:
                return Sort.by("id").descending();
            case CREATEDDATE_ASC:
                return Sort.by("createdDate").ascending();
            case CREATEDDATE_DESC:
                return Sort.by("createdDate").descending();
            case DESCRIPTION_ASC:
                return Sort.by("description").ascending();
            case DESCRIPTION_DESC:
                return Sort.by("description").descending();
            default:
                return Sort.by("name").ascending();
        }
    }

    private Artist convertArtistUpdateCommandToArtist(Artist oldArtist, ArtistUpdateCommand command) throws ParseException, IOException {
        oldArtist.setName(command.getName());
        oldArtist.setDescription(command.getDescription());
        oldArtist.setCreatedDate(new SimpleDateFormat("yyyy-MM-dd").parse(command.getCreatedDate()));
        oldArtist.setDeleted(command.isDeleted());
        if (command.getPhotoFile() != null) {
//            fileHelper.deleteFile(oldArtist.getPhotoUri(), FileOperations.ARTIST_PHOTO);
            oldArtist.setPhotoUri(fileHelper.getNewUri(command.getPhotoFile(), FileOperations.ARTIST_PHOTO));
        }
        return oldArtist;
    }

    public void subscribe(int id, int artistId) {
        Artist artist = artistsRepository.findById(artistId).orElseThrow(ResourceNotFoundException::new);
        artist.getSubscribers().add(usersRepository.getOne(id));
        artistsRepository.save(artist);
    }

    public void unsubscribe(int id, int artistId) {
        Artist artist = artistsRepository.findById(artistId).orElseThrow(ResourceNotFoundException::new);
        artist.getSubscribers().remove(usersRepository.getOne(id));
        artistsRepository.save(artist);
    }
}
