package com.khramykh.platform.application.searchApi;

import com.khramykh.platform.application.repositories.AlbumsRepository;
import com.khramykh.platform.application.repositories.ArtistsRepository;
import com.khramykh.platform.application.repositories.TracksRepository;
import com.khramykh.platform.domain.entities.Album;
import com.khramykh.platform.domain.entities.Artist;
import com.khramykh.platform.domain.entities.BaseEntity;
import com.khramykh.platform.domain.entities.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SearchService {
    @Autowired
    private TracksRepository tracksRepository;
    @Autowired
    private AlbumsRepository albumsRepository;
    @Autowired
    private ArtistsRepository artistsRepository;

    public Map<String, List<? extends BaseEntity>> search(String query) {
        List<Track> tracks = tracksRepository.getAllByNameContaining(query);
        List<Artist> artists = artistsRepository.getAllByNameContaining(query);
        List<Album> albums = albumsRepository.getAllByNameContaining(query);

        Map<String, List<? extends BaseEntity>> result = new HashMap<>();
        result.put("tracks", tracks);
        result.put("artists", artists);
        result.put("albums", albums);

        return result;
    }
}
