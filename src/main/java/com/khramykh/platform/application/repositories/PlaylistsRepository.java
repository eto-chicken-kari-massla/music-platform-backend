package com.khramykh.platform.application.repositories;

import com.khramykh.platform.domain.entities.Playlist;
import com.khramykh.platform.domain.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public interface PlaylistsRepository extends PagingAndSortingRepository<Playlist, Integer> {
    Page<Playlist> findPlaylistByNameContaining(String name, Pageable page);

    Page<Playlist> findPlaylistsByUser(User user, Pageable pageable);

    @Transactional
    void deleteAllByUserContaining(User user);
}
