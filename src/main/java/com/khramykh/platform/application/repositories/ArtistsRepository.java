package com.khramykh.platform.application.repositories;

import com.khramykh.platform.domain.entities.Artist;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ArtistsRepository extends PagingAndSortingRepository<Artist, Integer> {
    Page<Artist> findByNameContaining(String name, Pageable pageable);
    List<Artist> getAllByNameContaining(String name);

}
