package com.khramykh.platform.application.utils;

public class ImageSize {
    public static final int PROFILE_PHOTO_HEIGHT = 500;
    public static final int TRACK_PHOTO_HEIGHT = 250;
    public static final int ALBUM_PHOTO_HEIGHT = 250;
    public static final int ARTIST_PHOTO_HEIGHT = 250;
}
