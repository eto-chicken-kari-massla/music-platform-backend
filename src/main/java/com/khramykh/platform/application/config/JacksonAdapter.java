package com.khramykh.platform.application.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.IOException;

@Configuration
public class JacksonAdapter extends WebMvcConfigurerAdapter {

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        return new Jackson2ObjectMapperBuilder().failOnUnknownProperties(false).serializerByType(Page.class,
                new JsonPageSerializer());
    }

    public class JsonPageSerializer extends JsonSerializer<Object> {

        @Override
        public void serialize(Object obj, JsonGenerator jsonGen, SerializerProvider serializerProvider)
                throws IOException {

            ObjectMapper om = new ObjectMapper().disable(MapperFeature.DEFAULT_VIEW_INCLUSION)
                    .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
                    .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            jsonGen.writeStartObject();
            if(obj instanceof Page) {
                jsonGen.writeFieldName("size");
                jsonGen.writeNumber(((Page)obj).getSize());
                jsonGen.writeFieldName("number");
                jsonGen.writeNumber(((Page)obj).getNumber());
                jsonGen.writeFieldName("totalElements");
                jsonGen.writeNumber(((Page)obj).getTotalElements());
                jsonGen.writeFieldName("last");
                jsonGen.writeBoolean(((Page)obj).isLast());
                jsonGen.writeFieldName("totalPages");
                jsonGen.writeNumber(((Page)obj).getTotalPages());
                jsonGen.writeObjectField("sort", ((Page)obj).getSort());
                jsonGen.writeFieldName("first");
                jsonGen.writeBoolean(((Page)obj).isFirst());
                jsonGen.writeFieldName("numberOfElements");
                jsonGen.writeNumber(((Page)obj).getNumberOfElements());
                jsonGen.writeFieldName("content");
                jsonGen.writeRawValue(
                        om.writerWithView(serializerProvider.getActiveView()).writeValueAsString(((Page)obj).getContent()));
                jsonGen.writeEndObject();
            } else {
                jsonGen.writeRawValue(
                        om.writerWithView(serializerProvider.getActiveView()).writeValueAsString(obj));
                jsonGen.writeEndObject();
            }

        }

    }
}