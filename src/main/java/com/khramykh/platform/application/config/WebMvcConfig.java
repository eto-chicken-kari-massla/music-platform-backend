package com.khramykh.platform.application.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.khramykh.platform.application.utils.MailSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.*;

import java.io.IOException;

@Configuration
@EnableSpringDataWebSupport
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    @Value("${upload.path}")
    private String uploadPath;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH");
    }

    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**").allowedOrigins("http://localhost:8080");
            }
        };
    }

//    @Bean
//    public SimpleModule springDataPageModule() {
//        return new SimpleModule().addSerializer(Page.class, new JsonSerializer<Page>() {
//            @Override
//            public void serialize(Page value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
//                gen.writeStartObject();
//                gen.writeNumberField("totalElements",value.getTotalElements());
//                gen.writeNumberField("totalPages", value.getTotalPages());
//                gen.writeNumberField("number", value.getNumber());
//                gen.writeNumberField("size", value.getSize());
//                gen.writeBooleanField("first", value.isFirst());
//                gen.writeBooleanField("last", value.isLast());
//                gen.writeFieldName("content");
//                serializers.defaultSerializeValue(value.getContent(),gen);
//                gen.writeEndObject();
//            }
//        });
//    }

    @Bean
    public MailSender getMailSender() {
        return new MailSender();
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/albums/**")
                .addResourceLocations("file:///" + uploadPath + "/images/albums/");
        registry.addResourceHandler("/img/tracks/**")
                .addResourceLocations("file:///" + uploadPath + "/images/tracks/");
        registry.addResourceHandler("/img/users/**")
                .addResourceLocations("file:///" + uploadPath + "/images/users/");
        registry.addResourceHandler("/img/artists/**")
                .addResourceLocations("file:///" + uploadPath + "/images/artists/");
        registry.addResourceHandler("/tracks/**")
                .addResourceLocations("file:///" + uploadPath + "/tracks/");
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
