package com.khramykh.platform.application.config.security.oauth2;

import com.khramykh.platform.application.commons.behaviors.UserPrincipal;
import com.khramykh.platform.application.config.security.oauth2.user.OAuth2UserInfo;
import com.khramykh.platform.application.config.security.oauth2.user.OAuth2UserInfoFactory;
import com.khramykh.platform.application.exceptions.OAuth2AuthenticationProcessingException;
import com.khramykh.platform.application.repositories.UsersRepository;
import com.khramykh.platform.domain.commons.enums.Country;
import com.khramykh.platform.domain.commons.enums.Provider;
import com.khramykh.platform.domain.commons.enums.Role;
import com.khramykh.platform.domain.commons.enums.UserGender;
import com.khramykh.platform.domain.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User;
        if (oAuth2UserRequest.getClientRegistration().getRegistrationId().equals(Provider.vk.toString())) {
            oAuth2User = loadVkUser(oAuth2UserRequest);
        } else {
            oAuth2User = super.loadUser(oAuth2UserRequest);
        }


        try {
            return processOAuth2User(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());
        if (StringUtils.isEmpty(oAuth2UserInfo.getEmail())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        Optional<User> userOptional = usersRepository.findByEmailIgnoreCase(oAuth2UserInfo.getEmail());
        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
            if (!user.getProvider().equals(Provider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()))) {
                throw new OAuth2AuthenticationProcessingException("Looks like you're signed up with " +
                        user.getProvider() + " account. Please use your " + user.getProvider() +
                        " account to login.");
            }
            user = updateExistingUser(user, oAuth2UserInfo);
        } else {
            user = registerNewUser(oAuth2UserRequest, oAuth2UserInfo);
        }

        return UserPrincipal.create(user, oAuth2User.getAttributes());
    }

    private User registerNewUser(OAuth2UserRequest oAuth2UserRequest, OAuth2UserInfo oAuth2UserInfo) {
        User user = new User();

        user.setProvider(Provider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()));

        if (user.getProvider() == Provider.google) {
            user.setFirstName(oAuth2UserInfo.getFirstName());
            user.setLastName(oAuth2UserInfo.getLastName());
        } else if (user.getProvider() == Provider.github) {
            user.setFirstName(oAuth2UserInfo.getName());
            user.setLastName(oAuth2UserInfo.getName());
        } else if (user.getProvider() == Provider.vk) {
            user.setFirstName(oAuth2UserInfo.getFirstName());
            user.setLastName(oAuth2UserInfo.getLastName());
        } else {
            user.setFirstName(oAuth2UserInfo.getFirstName());
            user.setLastName(oAuth2UserInfo.getLastName());
        }
        user.setEmail(oAuth2UserInfo.getEmail());
        user.setPhotoUri(oAuth2UserInfo.getImageUrl());
        user.setCountry(Country.GERMANY);
        user.setGender(UserGender.MALE);
        user.setRoles(Collections.singleton(Role.USER));
        user.setBirthday(new Date(System.currentTimeMillis()));
        user.setCreatedBy(oAuth2UserInfo.getEmail());
        return usersRepository.save(user);
    }

    private User updateExistingUser(User existingUser, OAuth2UserInfo oAuth2UserInfo) {
        if (existingUser.getProvider() == Provider.google) {
            existingUser.setFirstName(oAuth2UserInfo.getFirstName());
            existingUser.setLastName(oAuth2UserInfo.getLastName());
        } else if (existingUser.getProvider() == Provider.github) {
            existingUser.setFirstName(oAuth2UserInfo.getName());
            existingUser.setLastName(oAuth2UserInfo.getName());
        } else if (existingUser.getProvider() == Provider.vk) {
            existingUser.setFirstName(oAuth2UserInfo.getFirstName());
            existingUser.setLastName(oAuth2UserInfo.getLastName());
        } else {
            existingUser.setFirstName(oAuth2UserInfo.getFirstName());
            existingUser.setFirstName(oAuth2UserInfo.getLastName());
        }
        existingUser.setPhotoUri(oAuth2UserInfo.getImageUrl());
        existingUser.setCountry(Country.GERMANY);
        existingUser.setGender(UserGender.MALE);
        return usersRepository.save(existingUser);
    }

    private OAuth2User loadVkUser(OAuth2UserRequest oAuth2UserRequest) {
        RestTemplate template = new RestTemplate();
        System.out.println(oAuth2UserRequest.getAccessToken().getTokenType());
        MultiValueMap<String, String> headers = new LinkedMultiValueMap();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", oAuth2UserRequest.getAccessToken().getTokenType().getValue() + " " + oAuth2UserRequest.getAccessToken().getTokenValue());
        HttpEntity<?> httpRequest = new HttpEntity(headers);
        String uri = oAuth2UserRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUri();
        String userNameAttributeName = oAuth2UserRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName();
        uri = uri.replace("{user_id}", userNameAttributeName + "=" + oAuth2UserRequest.getAdditionalParameters().get(userNameAttributeName));

        try {
            ResponseEntity<Object> entity = template.exchange(uri, HttpMethod.GET, httpRequest, Object.class);
            Map response = (Map) entity.getBody();
            ArrayList valueList = (ArrayList) response.get("response");
            Map<String, Object> userAttributes = (Map<String, Object>) valueList.get(0);
            userAttributes.put(userNameAttributeName, oAuth2UserRequest.getAdditionalParameters().get(userNameAttributeName));
            Set<GrantedAuthority> authorities = Collections.singleton(new OAuth2UserAuthority(userAttributes));
            return new DefaultOAuth2User(authorities, userAttributes, userNameAttributeName);

        } catch (HttpClientErrorException ex) {
            ex.printStackTrace();
            throw new OAuth2AuthenticationProcessingException(ex.getMessage());
        }
    }
}
