package com.khramykh.platform.application.aop;

import com.khramykh.platform.application.albumsApi.commands.AlbumCreateCommand;
import com.khramykh.platform.application.tracksApi.commands.TrackCreateCommand;
import com.khramykh.platform.application.utils.MailSender;
import com.khramykh.platform.domain.entities.Album;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MailSenderAspect {
    @Autowired
    private MailSender mailSender;

    @Pointcut("execution(public * com.khramykh.platform.application.albumsApi.AlbumsService.create(..)) && args(command,..)")
    public void sendNotificationOnAlbumRelease(AlbumCreateCommand command) {
    }

    @After("sendNotificationOnAlbumRelease(command)")
    public void sendNotificationOnAlbumReleaseHandler(AlbumCreateCommand command) {
        System.out.println("Album_name - " + command.getName());
    }

//    @AfterReturning(
//            pointcut = "sendNotificationOnAlbumRelease(command) && cflow(sendNotificationOnAlbumReleaseHandler(command))",
//            returning = "album"
//    )
//    public void afterSendNotificationOnAlbumRelease(JoinPoint joinPoint, AlbumCreateCommand command, Album album) {
//        System.out.println("AlbumID = " + album.getId());
//    }

    @Pointcut("execution(public * com.khramykh.platform.application.tracksApi.TracksService.create(..)) && args(command,..)")
    public void sendNotificationOnTrackRelease(TrackCreateCommand command) {
    }

    @After("sendNotificationOnTrackRelease(command)")
    public void sendNotificationOnTrackReleaseHandler(TrackCreateCommand command) {
        System.out.println("Track_name - " + command.getName());
    }

}
