package com.khramykh.platform.application.playlistsApi;

import com.khramykh.platform.application.commons.sort.PlaylistSort;
import com.khramykh.platform.application.exceptions.ForbiddenException;
import com.khramykh.platform.application.exceptions.ResourceNotFoundException;
import com.khramykh.platform.application.exceptions.UserNotFoundException;
import com.khramykh.platform.application.playlistsApi.commands.PlaylistCreateCommand;
import com.khramykh.platform.application.playlistsApi.commands.PlaylistUpdateCommand;
import com.khramykh.platform.application.repositories.PlaylistsRepository;
import com.khramykh.platform.application.repositories.TracksRepository;
import com.khramykh.platform.application.repositories.UsersRepository;
import com.khramykh.platform.domain.entities.Playlist;
import com.khramykh.platform.domain.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PlaylistsService {

    @Autowired
    private PlaylistsRepository playlistsRepository;
    @Autowired
    private TracksRepository tracksRepository;
    @Autowired
    private UsersRepository usersRepository;

    public User getUserByEmail(String email) {
        return usersRepository.findByEmailIgnoreCase(email).orElseThrow(UserNotFoundException::new);
    }

    public Page<Playlist> getPlaylistsByUser(int pageNum, int pageSize, String email, PlaylistSort playlistSort) {
        User user = getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            Page<Playlist> playlists = playlistsRepository.findPlaylistsByUser(user, PageRequest.of(pageNum, pageSize, getSortType(playlistSort)));
            return playlists;
        }
    }

    public void removeById(int playlistId, String email) {
        User user = getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            Playlist playlist = playlistsRepository.findById(playlistId).orElseThrow(ResourceNotFoundException::new);
            if (playlist != null) {
                if (!playlist.getUser().getId().equals(user.getId())) {
                    throw new ForbiddenException();
                }
                playlistsRepository.delete(playlist);
            }
        }
    }

    public Playlist update(PlaylistUpdateCommand command, String email) {
        User user = getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        }
        Playlist playlist = playlistsRepository.findById(command.getId()).orElseThrow(ResourceNotFoundException::new);
        playlist.setName(command.getName());
        return playlistsRepository.save(playlist);
    }

    public Playlist create(PlaylistCreateCommand command, String email) {
        User user = getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        }
        Playlist playlist = new Playlist(command.getName(), user);
        return playlistsRepository.save(playlist);
    }

    public void addTrackToPlaylist(int playlistId, int trackId, String email) {
        User user = getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            playlistsRepository.findById(playlistId).map(playlist -> {
                if (playlist.getUser().getId().equals(user.getId())) {
                    playlist.getTracks().add(tracksRepository.getOne(trackId));
                    return playlistsRepository.save(playlist);
                } else {
                    throw new ForbiddenException();
                }
            });
        }
    }

    public void removeTrackFromPlaylist(int playlistId, int trackId, String email) {
        User user = getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            Playlist playlist = playlistsRepository.findById(playlistId).orElseThrow(ResourceNotFoundException::new);
            if (playlist.getUser().getId().equals(user.getId())) {
                playlist.getTracks().remove(tracksRepository.getOne(trackId));
                playlistsRepository.save(playlist);
            }
            throw new ForbiddenException();
        }
    }

    public Playlist getPlaylistById(int playlistId, String email) {
        User user = getUserByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        } else {
            Playlist playlist = playlistsRepository.findById(playlistId).orElseThrow(ResourceNotFoundException::new);
            if (playlist.getUser().getId().equals(user.getId())) {
                return playlist;
            }
            throw new ForbiddenException();
        }
    }

    private Sort getSortType(PlaylistSort playlistSort) {
        switch (playlistSort) {
            case NAME_DESC:
                return Sort.by("name").descending();
            case NAME_ASC:
                return Sort.by("name").ascending();
            case ID_ASC:
                return Sort.by("id").ascending();
            default:
                return Sort.by("id").descending();
        }
    }
}
