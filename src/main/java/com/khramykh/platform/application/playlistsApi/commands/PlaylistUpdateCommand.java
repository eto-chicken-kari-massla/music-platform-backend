package com.khramykh.platform.application.playlistsApi.commands;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistUpdateCommand {
    private int id;
    private String name;
}
