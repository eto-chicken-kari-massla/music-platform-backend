package com.khramykh.platform.application.usersApi.commands;

import com.khramykh.platform.domain.commons.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateCommand {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String birthday;
    private String gender;
    private String country;
    private String password;
    private Set<Role> roles;
}
