package com.khramykh.platform.domain.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.khramykh.platform.api.commons.JsonViews;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Nationalized;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "track_categories")
public class Category extends BaseEntity {
    @JsonView({JsonViews.UI.TrackView.class})
    @Nationalized
    private String name;
    @Nationalized
    private String description;
}
