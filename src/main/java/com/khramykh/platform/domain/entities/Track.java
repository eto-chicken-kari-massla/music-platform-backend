package com.khramykh.platform.domain.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.khramykh.platform.api.commons.JsonViews;
import com.khramykh.platform.domain.commons.enums.TrackTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tracks")
public class Track extends BaseEntity {
    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private String name;

    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.SearchView.class, JsonViews.UI.SearchView.class})
    @Enumerated(EnumType.STRING)
    @Nationalized
    private TrackTypes type;

    @JsonView(JsonViews.UI.TrackView.class)
    @Nationalized
    private String description;

    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private String photoUri;

    @JsonView(JsonViews.UI.TrackView.class)
    @Nationalized
    private String trackUri;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private Date releaseDate;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinColumn(name = "category_id", nullable = false)
    @JoinTable(
            name = "category_track",
            joinColumns = @JoinColumn(name = "track_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    @JsonView(JsonViews.UI.TrackView.class)
    @Nationalized
    private Set<Category> categories = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "album_id")
    @JsonView(JsonViews.UI.TrackView.class)
    @Nationalized
    private Album album;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(
            name = "atrist_track",
            joinColumns = @JoinColumn(name = "track_id"),
            inverseJoinColumns = @JoinColumn(name = "artist_id", nullable = false)
    )
    @JsonView(JsonViews.UI.TrackView.class)
    @Nationalized
    private Set<Artist> artists = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(
            name = "track_likes",
            joinColumns = {@JoinColumn(name = "track_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    @JsonView(JsonViews.UI.TrackView.class)
    @Nationalized
    private Set<User> likes = new HashSet<>();
}
