package com.khramykh.platform.domain.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.khramykh.platform.api.commons.JsonViews;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "artists")
public class Artist extends BaseEntity {
    @NotNull
    @NotEmpty
    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.AlbumView.class, JsonViews.UI.ArtistView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private String name;

    @NotNull
    @NotEmpty
    @JsonView({JsonViews.UI.AlbumView.class, JsonViews.UI.ArtistView.class})
    @Nationalized
    private String description;

    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.AlbumView.class, JsonViews.UI.ArtistView.class, JsonViews.UI.SearchView.class})
    private String photoUri;

    @ManyToMany
    @JoinTable(
            name = "artists_subscriptions",
            joinColumns = {@JoinColumn(name = "artist_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    @Nationalized
    private Set<User> subscribers = new HashSet<>();
}
