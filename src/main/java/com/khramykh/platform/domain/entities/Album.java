package com.khramykh.platform.domain.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.khramykh.platform.api.commons.JsonViews;
import com.khramykh.platform.domain.commons.enums.AlbumTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "albums")
public class Album extends BaseEntity {

    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.AlbumView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private String name;

    @JsonView(JsonViews.UI.AlbumView.class)
    @Nationalized
    private String description;

    @Enumerated(EnumType.STRING)
    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.AlbumView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private AlbumTypes type;

    @JsonView({JsonViews.UI.TrackView.class, JsonViews.UI.AlbumView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private String photoUri;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView({JsonViews.UI.AlbumView.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private Date releaseDate;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @CollectionTable(name = "artist_album", joinColumns = @JoinColumn(name = "album_id"))
    @JoinColumn(name = "artist_id", nullable = false)
    @JsonView(JsonViews.UI.AlbumView.class)
    @Nationalized
    private Set<Artist> artists = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "album_likes",
            joinColumns = {@JoinColumn(name = "album_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    @JsonView(JsonViews.UI.AlbumView.class)
    @Nationalized
    private Set<User> likes = new HashSet<>();
}
