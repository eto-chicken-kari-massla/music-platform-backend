package com.khramykh.platform.domain.entities;

import com.khramykh.platform.domain.commons.enums.Country;
import com.khramykh.platform.domain.commons.enums.Provider;
import com.khramykh.platform.domain.commons.enums.Role;
import com.khramykh.platform.domain.commons.enums.UserGender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends BaseEntity {
    @NotNull(message = "{user.firstnamenotnull}")
    @NotEmpty(message = "{user.firstnamenotempty}")
    @Nationalized
    private String firstName;

    @NotNull(message = "{user.lastnamenotnull}")
    @NotEmpty(message = "{user.lastnamenotempty}")
    @Nationalized
    private String lastName;

    @Email(message = "{user.incorrectemail}")
    @NotNull(message = "{user.emailnotnull}")
    @NotEmpty(message = "{user.emailnotempty}")
    @Nationalized
    private String email;

    @NotNull(message = "{user.birthdaynotnull}")
    @PastOrPresent
    @Nationalized
    private Date birthday;

    @Nationalized
    private String photoUri;

    @Enumerated(EnumType.STRING)
    @Nationalized
    private Provider provider;

    @Column(name = "activation_code")
    @Nationalized
    private String activationCode;

    @NotNull(message = "{user.gendernotnull}")
    @Enumerated(EnumType.STRING)
    @Nationalized
    private UserGender gender;

    @NotNull(message = "{user.countrynotnull}")
    @Enumerated(EnumType.STRING)
    @Nationalized
    private Country country;

    @ManyToMany
    @JoinTable(
            name = "artists_subscriptions",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "artist_id")}
    )
    @Nationalized
    private Set<Artist> subscriptions  = new HashSet<>();

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    @Nationalized
    private Set<Role> roles = new HashSet<>();

    @Nationalized
    private String hashPassword;
}
