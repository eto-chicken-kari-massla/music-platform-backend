package com.khramykh.platform.domain.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.khramykh.platform.api.commons.JsonViews;
import lombok.Data;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
@Data
public abstract class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView({JsonViews.UI.class, JsonViews.UI.SearchView.class})
    @Nationalized
    private Integer id;

    @Version
    @JsonView({JsonViews.UI.class})
    @Nationalized
    private Integer version;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonView({JsonViews.UI.class})
    @Nationalized
    private Date createdDate;

    @CreatedBy
    @JsonView({JsonViews.UI.class})
    @Nationalized
    private String createdBy;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonView({JsonViews.UI.class})
    @Nationalized
    private Date lastModifiedDate;

    @LastModifiedBy
    @JsonView({JsonViews.UI.class})
    @Nationalized
    private String lastModifiedBy;

    @Column(name = "deleted")
    @JsonView({JsonViews.UI.class})
    @Nationalized
    private boolean deleted = false;
}
