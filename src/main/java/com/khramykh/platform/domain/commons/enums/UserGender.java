package com.khramykh.platform.domain.commons.enums;

public enum UserGender {
    MALE, FEMALE
}
