package com.khramykh.platform.domain.commons.enums;

public enum Provider {
    local,
    facebook,
    google,
    github,
    vk
}
