package com.khramykh.platform.domain.commons.enums;

public enum TrackTypes {
    MUSIC_TRACK_ALBUM,
    MUSIC_TRACK_SINGLE,
    PODCAST_ALBUM,
    PODCAST_SINGLE
}
