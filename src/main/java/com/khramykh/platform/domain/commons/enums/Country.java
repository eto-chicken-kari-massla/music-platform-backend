package com.khramykh.platform.domain.commons.enums;

public enum Country {
    BELARUS,
    USA,
    GERMANY,
    JAPAN,
    RUSSIA,
    UK,
    POLAND,
    FRANCE,
    CHINA,
    FINLAND
}
