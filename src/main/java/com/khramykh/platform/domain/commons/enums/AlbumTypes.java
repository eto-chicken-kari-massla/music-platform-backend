package com.khramykh.platform.domain.commons.enums;

public enum AlbumTypes {
    TRACK_ALBUM,
    PODCAST_ALBUM,
}
