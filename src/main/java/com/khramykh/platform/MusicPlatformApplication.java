package com.khramykh.platform;

import com.khramykh.platform.application.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableConfigurationProperties(AppProperties.class)
@EnableAspectJAutoProxy
public class MusicPlatformApplication {
    public static void main(String[] args) {
        SpringApplication.run(MusicPlatformApplication.class, args);
    }
}
